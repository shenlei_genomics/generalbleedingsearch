# -*- coding: utf-8 -*-
"""
Created on Mon Jul 16 11:42:23 2018

@author: shenlei
"""
from __future__ import print_function
from __future__ import division

import os
from re import split
###### Version and Date
prog_version = '0.1.0'
prog_date = '2018-07-16'

#######################################################################
############################  BEGIN Class  ############################
#######################################################################
class BFSearcher(object):
    """This class create a instance to search optimized bleeding factor."""
    def __init__(self, cfgFile):
        # super(SimulateImg, self).__init__()
        ## import lib
        import datetime

        self.dt = datetime
        # load config from JSON file
        self.cfg = self.loadConfig(cfgFile)
        self.logfn = self.cfg["process"]["logfile"]
        self.settingfn = os.path.join(self.cfg["process"]["basecallpath"], "settings.config")
        self.trackextend = self.cfg["process"]["trackextend"]
        self.target = self.cfg['process']['target']
        self.fqpath = os.path.join(self.cfg["process"]["basecallpath"],'OutputFq')      
        self.analysisfn = os.path.join(self.cfg["process"]["analysisAndMappingPath"], 'analysisAndMapping.py')
        self.mappingtype = self.cfg["process"]["mappingtype"]
        mappingdict = {1:'-m -r Ecoli.fa', 2:'-m -r Human.fa', 3:'-m -r hg19.fa'}
        if self.mappingtype not in mappingdict.keys():
            self._logger("Error: mapping type should be 1 2 or 3!")
            raise NameError("Error: mapping type should be 1 2 or 3!")
        self.mappingAppendCmd = mappingdict[self.mappingtype]
        workspace_fq_path = self.getWorkspaceAndFqPath()
        if len(workspace_fq_path) != 2:
            self._logger("Error: workspace and fq path found not correct in setting.config!")
            raise IOError("Error: workspace and fq path found not correct in setting.config!")
        self.wspath,self.fqpath = workspace_fq_path
        self.rst_list = []
        self.max_mr = 0
        self.max_mr_param= (0,0,0,0)
        
    def _logger(self, msg):
        timeNow = self.dt.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        with open(self.logfn, 'a') as fh:
            print("[%s] %s" % (timeNow, msg), file=fh)
    
    def getWorkspaceAndFqPath(self):
        with open(self.settingfn) as fh:
            lines = fh.readlines()
        workspace_fq_path = [] 
        flag = ['modparam.gpupipeline.saveintensityPath','modparam.basecall.generateFastqPath']
        for line in lines:
            for perflag in flag:
                if perflag in line and line[0]!='#':
                    parts = split('[ #=]',line)
                    for part in parts:
                        if '/' in part or '\\' in part:
                            workspace_fq_path.append(part)
                            break
        self._logger(str(workspace_fq_path))
        return workspace_fq_path                            
                
    
    #for basecall CPU  version, the fn is self.settingfn
    def changeSetting(self, r,k,b):
        with open(self.settingfn) as fh:
            lines = fh.readlines()
        newlines=[]
        for line in lines:
            newline=line
            if 'modparam.basecall.pixel_radius' in line and line[0]!='#':
                newline='modparam.basecall.pixel_radius={0}\n'.format(r)
            if 'modparam.basecall.bleedingFactor_read1[]' in line and line[0]!='#':
                kb_str='{0} {1} '.format(k,b)*4
                newline='modparam.basecall.bleedingFactor_read1[]={0}\n'.format(kb_str)
            if 'modparam.basecall.trackExtend' in line and line[0]!='#':
                newline = 'modparam.basecall.trackExtend = {0}\n'.format(self.trackextend)
            newlines.append(newline)
        with open(self.settingfn,'w') as fh:
            fh.writelines(newlines)  
    
    def loadConfig(self, cfgFile):
        import json
        with open(cfgFile, 'r') as fh:
            jsCfg = json.load(fh)
            if 'process' not in jsCfg:
                jsCfg["process"] = {}
        return jsCfg
    
    def getTarget(self, reportfn):
        mark = 'var summaryTable ='
        targetlist = []
        if self.target.lower() == 'mappingrate':
            targetlist = ['MappingRate%','Mappingrate%','MappingRate(%)','Mappingrate(%)']
        elif self.target.lower() == 'q30':
            targetlist = ['q30%','Q30%','Q30(%)','q30(%)']
        elif self.target.lower() == 'avgerrorrate':
            targetlist = ['AvgErrorRate%','AvgErrorRate(%)']
        else:
            self._logger('Input target '+self.target + ' error!' + 'Should be Mappingrate or Q30 or AvgErrorRate!')
            raise NameError('Input target '+self.target + ' error!' + 'Should be Mappingrate or Q30 or AvgErrorRate!')
        if not os.path.exists(reportfn):
            self._logger(reportfn + " not exist!")
            return None
        with open(reportfn) as fh:
            lines = fh.readlines()
        for line in lines:
            if mark in line:
                parts = line.replace(';','').split('=')
                mr_list=eval(parts[1])
                for mr in mr_list:
                    if mr[0] in targetlist:
                        return float(mr[1])
    
    def runSingleBF(self,r,k,b):
        print('r = {0}\t k = {1}\t b = {2}\n'.format(r,k,b))
        self.changeSetting(r,k,b)
        rst_name = 'C{0}R{1}_r_{2}_k{3}_b{4}'.format(self.cfg['process']['FOVCol'],self.cfg['process']['FOVRow'],r,k,b)
        cmd_str='client.exe {0} {1} {2} {3} -S -N {4}'.format(self.cfg['process']['datapath'],self.cfg['process']['cycle'], 
                            self.cfg['process']['FOVCol'],self.cfg['process']['FOVRow'],rst_name)
        os.chdir(self.cfg['process']['basecallpath'])
        os.system('start processor.exe')
        self._logger(cmd_str)
        os.system(cmd_str)
        os.system("taskkill /im processor.exe -f")
        mapping_str = '{0} {1} {2} {3} {4}'.format(self.analysisfn,os.path.join(self.wspath, rst_name,'L01','metrics'),os.path.join(self.fqpath,rst_name,'L01'),rst_name+'_'+'L01',self.mappingAppendCmd)
        self._logger(mapping_str)
        os.system(mapping_str)
        mr_fn= os.path.join(self.fqpath,rst_name,'L01',rst_name+'_L01.summaryReport.html') #r'E:\SL\Release_V2_715_1stbf\Release_V2_715_1stbf\OutputFq\{0}\L01\{0}_L01.summaryReport.html'.format(rst_name)
        mr=self.getTarget(mr_fn)
        if mr==None:
            mr = 0    
        self._logger('r:{0}\tk:{1}\tb:{2}\t{3}:{4}\n'.format(r,k,b,self.target,mr))    
        return mr

    def runGridding(self):
        from numpy import arange
        rstart,rend,rstep = self.cfg['process']['r']
        kstart,kend,kstep = self.cfg['process']['k']
        bstart,bend,bstep = self.cfg['process']['b']
        for r in list(arange(rstart,rend,rstep)):
            for k in list(arange(kstart,kend,kstep)):
                for b in list(arange(bstart,bend,bstep)):
                    print('-'*53)
                    print('Running r = {0}  k = {1}  b = {2}'.format(r,k,b))
                    mr = self.runSingleBF(r, k, b)
                    print('Result:{0}'.format(mr))
                    self.rst_list.append([r,k,b,mr])    
                    if mr > self.max_mr:
                        self.max_mr = mr
                        self.max_mr_param = (mr,r,k,b)
        print('-'*53)
        print('Final result -- max {2}:{0}  params:{1}'.format(self.max_mr,self.max_mr_param, self.target))
        self._logger('Final result -- max {2}:{0}  params:{1}'.format(self.max_mr,self.max_mr_param, self.target))
        lines='--------------All running result------------------------------\n'
        lines += 'r,k,b,{0}\n'.format(self.target)
        for result in self.rst_list:
            lines += ','.join(str(x) for x in result)
            lines += '\n'
        with open(self.logfn,'a') as fh:
            fh.writelines(lines)
    
    def runBayes(self):
        from bayes_opt import BayesianOptimization        
        rstart,rend,rstep = self.cfg['process']['r']
        kstart,kend,kstep = self.cfg['process']['k']
        bstart,bend,bstep = self.cfg['process']['b']
        gp_params = {"alpha": 1e-5}        
        mrBO = BayesianOptimization(self.runSingleBF,
            {'r': (rstart, rend), 'k': (kstart, kend), 'b':(bstart,bend)})
        #mrBO.initialize(
        #{
        #    'target': [0.7548, 0.7903,0.1051],
        #    'r': [0.7, 0.9,1.93],
        #    'k': [0.1, 0.15,0.18],
        #    'b': [0.005,0.009,0.01]
        #})    
        #mrBO.explore({'r': [0.9, 1.0], 'k': [0.16, 0.16], 'b':[0.005,0.009]})
        mrBO.maximize(n_iter=25, **gp_params)
        print('-' * 53)
        print('Final Results')
        print('mr: %f' % mrBO.res['max'])
        self._logger('-' * 53)
        self._logger('Final Results')
        self._logger('mr: %f' % mrBO.res['max'])
        print('-'*53)
        print('All: %f' % mrBO.res['all'])
        self._logger('-' * 53)
        self._logger('All: %f' % mrBO.res['all'])

    def run(self):
        opt_method = self.cfg['process']['optimization']              
        if opt_method == 'gridding':
            self.runGridding()
        if opt_method == 'Bayes':
            try:                
                self.runBayes()
            except ImportError:
                self._logger('Error: bayes-optimization module not found!')
                print('Error: bayes-optimization module not found!')

#################################
##
##   Main function of program.
##
#################################
def main():
    ###### Usage
    import sys
    usage = '''

    Version %s  by Lei  %s

    Usage: %s <config> >STDOUT
    ''' % (prog_version, prog_date, os.path.basename(sys.argv[0]))

    ######################### Phrase parameters #########################
    import argparse
    ArgParser = argparse.ArgumentParser(usage=usage)
    ArgParser.add_argument("--version", action="version", version=prog_version)

    (para, args) = ArgParser.parse_known_args()

    if len(args) != 1:
        ArgParser.print_help()
        print("\nERROR: The parameters number is not correct!", file=sys.stderr)
        sys.exit(1)
    else:
        (config,) = args

    ############################# Main Body #############################
    searcher = BFSearcher(config)
    searcher.run()
    
#################################
##
##   Start the main program.
##
#################################
if __name__ == '__main__':
    main()

################## God's in his heaven, All's right with the world. ##################
                    
    
        
    
    
        
    